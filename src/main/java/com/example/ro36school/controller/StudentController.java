package com.example.ro36school.controller;

import com.example.ro36school.dto.StudentCreateDTO;
import com.example.ro36school.dto.StudentDTO;
import com.example.ro36school.service.StudentService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.InvalidParameterException;
import java.util.List;

// punctul de intrare în aplicația noastră pentru toate request-urile exterioare
// path-urile sunt mapate automat de Spring
@Controller
@RequestMapping("/api/students")
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }


    // GET http://lolcalhost:8080/api/students <- returnează toți studenții
    @GetMapping
    public ResponseEntity<List<StudentDTO>> findAll(@RequestParam(value = "first_name", required = false) String firstName) {
        if (firstName != null) {
            return ResponseEntity.ok(studentService.findAllByFirstName(firstName));
        } else {
            return ResponseEntity.ok(studentService.findAll());
        }
    }

    // GET http://lolcalhost:8080/api/students/5 <- returnează studentul cu id-ul 5
    @GetMapping("/{id}")
    public ResponseEntity<StudentDTO> findById(@PathVariable(name = "id") Integer idParam) {
        return ResponseEntity.ok(studentService.findById(idParam));
    }

    // POST http://localhost:8080/api/students
    @PostMapping
    public ResponseEntity create(@RequestBody StudentCreateDTO createDTO) {
        try {
            // success return the created student
            StudentDTO createdDto = studentService.createStudent(createDTO);
            return ResponseEntity.ok(createdDto);
        } catch (UnsupportedOperationException exception) {
            // failure - something went wrong - return the cause
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable("id") Integer idParam, @RequestBody StudentDTO studentDTO) {
        if (idParam.equals(studentDTO.getId())) {
            return ResponseEntity.ok(studentService.updateStudentWithoutPassword(studentDTO));
        } else {
            return ResponseEntity
                    .badRequest()
                    .body("Path parameter id: " + idParam + " does not match body parameter id: " + studentDTO.getId());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Integer idParam) {
        try {
            studentService.deleteStudent(idParam);
            return ResponseEntity.ok().body("Successfully deleted student with id:" + idParam);
        } catch (UnsupportedOperationException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    // http://www.localhost:8080/api/students/15/grades/matematica
    @GetMapping("/{classId}/grades/{subjectName}")
    public ResponseEntity getGrades(@PathVariable Integer classId, @PathVariable String subjectName) {
        return null;
    }

    // http://www.localhost:8080/api/students/15/grades?subject=matematica&year=2020&semester=2
    @GetMapping("/{classId}/grades")
    public ResponseEntity getGradesWithPatameters(@PathVariable Integer classId,
                                                  @RequestParam("subject") String subjectName,
                                                  @RequestParam("year") Integer year,
                                                  @RequestParam("semester") Integer semester) {
        return null;
    }

    // când folosim @RestController pe clasă Spring va face wrap automat într-un ResponseEntity la ce returnează funcția

//    @GetMapping("/{id}")
//    public StudentDTO findById(@PathVariable(name = "id") Integer idParam) {
//        return studentService.findById(idParam);
//    }
}


// http request/response main components: http call type, url, headers (includes http status), body


// REST
// ca să luăm toate entitățile de un tip student - http call GET pe ".../students"
// ca să luăm o entitate de un tip student - http call GET pe ".../students/{id}"
// ca să creăm o entitate de tip student - http call POST pe ".../students" și body (cu body în care punem informația despre entitate)
// ca să edităm o entitate de tip student - http call PUT pe ".../students/{id} și body (cu body în care punem informația despre entitate)
// ca să ștergem o entitate de student - http call DELETE pe ".../students/{id}