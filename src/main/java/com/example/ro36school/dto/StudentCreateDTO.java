package com.example.ro36school.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
// clasă folosită pentru oglindirea body-ului dintr-un request http care vine către aplicația noastră
public class StudentCreateDTO {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private Integer classId;
    private String email;
    private String password;
}
