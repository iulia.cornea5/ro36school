package com.example.ro36school.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.time.LocalDate;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
// clasă folosită pentru oglindirea body-ului dintr-un răspuns http pe care aplicația noastră îl oferă

public class StudentDTO {

    private int id;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private ClassDTO classDTO;
    private String email;
}
