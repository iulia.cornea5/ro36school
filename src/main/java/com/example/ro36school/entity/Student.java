package com.example.ro36school.entity;

import com.example.ro36school.entity.enums.Nationality;
import jdk.jfr.DataAmount;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity(name = "students")
// Clasă care oglindește tabelul din baza de date
// Tine informații despre un student (o linie din tabel)
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String firstName;

    private String lastName;

    @Column(name = "birth_date")
    private LocalDate dateOfBirth;

    private Integer classId;

    private String email;

    private String password;

//    @Enumerated(EnumType.STRING)
//    private Nationality nationality;


}
