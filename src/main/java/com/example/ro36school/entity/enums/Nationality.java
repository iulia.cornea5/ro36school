package com.example.ro36school.entity.enums;

public enum Nationality {

    // 1  ,     2  ,     3   ,    4    EnumType.ORDINAL
//  FRENCH, ENGLISH, ROMANIAN, HUNGARIAN EnumType.STRING
    FRENCH, ENGLISH, ROMANIAN, HUNGARIAN
}

