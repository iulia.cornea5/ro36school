package com.example.ro36school.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class Ro36WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService ro36UserDetailsService;
    private final PasswordEncoder ro36PasswordEncoder;

    // ro36UserDetailsService vine de bean-ul creat din serviciul Ro36UserDetailsService
    // ro36PasswordEncoder vine din bean-ul creat de metoda getRo36PasswordEncoder din clasa Ro36PasswordConfig
    public Ro36WebSecurityConfig(UserDetailsService ro36UserDetailsService, PasswordEncoder ro36PasswordEncoder) {
        this.ro36UserDetailsService = ro36UserDetailsService;
        this.ro36PasswordEncoder = ro36PasswordEncoder;
    }

    // configureaza partea de autorizare pentru acces la pagini/resurse/url
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // ca să putem face si call-uri de POST din frontend
                .csrf().disable()
                // ce request-uri să fie accesibilie (autorizate)
                .authorizeRequests()
                // request-urile care vin pe path-urile de mai jos au permisiunea de a fi accesate fără ca un user să fie logat
                .antMatchers("/info").permitAll()
                // orice alt request
                .anyRequest()
                // va trebui să fie un user autentificat
                .authenticated()
                // prin autentificare de httpBasic (adică cu username și parolă)
                .and().httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        DaoAuthenticationProvider ro36AuthenticationProvider = new DaoAuthenticationProvider();
        ro36AuthenticationProvider.setPasswordEncoder(ro36PasswordEncoder);
        ro36AuthenticationProvider.setUserDetailsService(ro36UserDetailsService);
        // foloseste authenticationProvider-ul creat de mine, care e costumizat cu clasele de care are aplicatia mea nevoie
        authenticationManagerBuilder.authenticationProvider(ro36AuthenticationProvider);
    }
}
