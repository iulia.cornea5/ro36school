package com.example.ro36school.service;

import com.example.ro36school.dto.StudentCreateDTO;
import com.example.ro36school.dto.StudentDTO;
import com.example.ro36school.entity.Student;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

// logica de funcționare a aplicație pentru entitate respectivă
public interface StudentService {

    List<StudentDTO> findAll();

    List<StudentDTO> findAllByClassId(Integer classId);

    StudentDTO findById(Integer id);

    List<StudentDTO> findAllByFirstName(String firstName);

    StudentDTO createStudent(StudentCreateDTO createDTO) throws UnsupportedOperationException;

    StudentDTO updateStudentWithoutPassword(StudentDTO studentDTO);

    Integer deleteStudent(Integer id) throws UnsupportedOperationException;
}
