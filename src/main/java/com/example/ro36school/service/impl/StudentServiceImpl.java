package com.example.ro36school.service.impl;

import com.example.ro36school.dto.StudentCreateDTO;
import com.example.ro36school.dto.StudentDTO;
import com.example.ro36school.entity.Student;
import com.example.ro36school.mapper.StudentMapper;
import com.example.ro36school.repository.StudentRepository;
import com.example.ro36school.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

// logica de funcționare a aplicație pentru entitate respectivă
@Service
public class StudentServiceImpl implements StudentService {

    private static Logger LOGGER = LoggerFactory.getLogger(StudentServiceImpl.class);

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;

    public StudentServiceImpl(StudentRepository studentRepository, StudentMapper studentMapper) {
        this.studentRepository = studentRepository;
        this.studentMapper = studentMapper;
    }

    @Override
    public List<StudentDTO> findAll() {
//        return studentRepository.findAll().stream().map(studentMapper::toDto).collect(Collectors.toList());
        return studentRepository.findAll().stream().map(s -> studentMapper.toDto(s)).collect(Collectors.toList());
    }

    @Override
    public List<StudentDTO> findAllByClassId(Integer classId) {

        return null;
    }

    @Override
    public StudentDTO findById(Integer id) {
        return studentMapper.toDto(studentRepository.findById(id).get());
    }

    @Override
    public List<StudentDTO> findAllByFirstName(String firstName) {
        return studentRepository.findByFirstName(firstName).stream().map(studentMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public StudentDTO createStudent(StudentCreateDTO createDTO) throws UnsupportedOperationException {
        if (studentRepository.existsByEmail(createDTO.getEmail())) {
            throw new UnsupportedOperationException("User with given email: " + createDTO.getEmail() + " already exists.");
        }
        Student studentToSave = studentMapper.toEntity(createDTO);
        Student savedStudent = studentRepository.save(studentToSave);
        StudentDTO studentDTO = studentMapper.toDto(savedStudent);
        return studentDTO;
    }

    @Override
    public StudentDTO updateStudentWithoutPassword(StudentDTO studentDTO) {

        Optional<Student> studentOptional = studentRepository.findById(studentDTO.getId());
        if (studentOptional.isPresent()) {

            Student old = studentOptional.get();
            old.setFirstName(studentDTO.getFirstName());
            old.setLastName(studentDTO.getLastName());
            old.setEmail(studentDTO.getEmail());
            old.setDateOfBirth(studentDTO.getDateOfBirth());
            old.setClassId(studentDTO.getClassDTO().getId());
            Student updated = studentRepository.save(old);
            return studentMapper.toDto(updated);
        } else {
            throw new UnsupportedOperationException("Invalid id: " + studentDTO.getId() + " for update student.");
        }
    }

    @Override
    public Integer deleteStudent(Integer id) throws UnsupportedOperationException {
        if (studentRepository.existsById(id)) {
            studentRepository.deleteById(id);
            LOGGER.info("Deleted student entity with id:" + id);
            return id;
        } else {
            LOGGER.error("Delete student entity failed. Invalid id: " + id + " for deleting student entity");
            throw new UnsupportedOperationException(
                    "Invalid id: " + id + " for deleting student entity");
        }
    }
}
